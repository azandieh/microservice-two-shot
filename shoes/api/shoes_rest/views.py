from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
        "id",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
        "id",
    ]

    encoders = {
        "bin": BinVOEncoder(),
    }

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
        "id",
    ]

    econders = {
        "bin": BinVOEncoder(),
    }

    def get_extra_data(self, o):
        return {"bin": o.bin.id}


# @require_http_methods(["GET", "POST"])
# def api_list_shoes(request):

#     if request.method == "GET":
#         shoes = Shoe.objects.all()
#         return JsonResponse(
#             {"shoes": shoes},
#             encoder=ShoeListEncoder,
#         )
#     else:
#         content = json.loads(request.body)
#         shoe = Shoe.objects.create(**content)
#         return JsonResponse(
#             shoe,
#             encoder=ShoeListEncoder,
#             safe=False,
#         )



@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(id=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, id):

    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=id)

            props = ["closet_name", "shelf_number", "section_number"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response








# # @require_http_methods(["GET", "POST"])
# # def api_list_shoes(request):
# #     if request.method == "GET":
# #         shoes = Shoe.objects.all()
# #         return JsonResponse(
# #             {"shoes": shoes},
# #             encoder=ShoeListEncoder,
# #         )
# #     else:
# #         content = json.loads(request.body)
# #         shoes = Shoe.objects.create(**content)
# #         return JsonResponse(
# #             shoes,
# #             encoder=ShoeListEncoder,
# #             safe=False,
# #         )


# @require_http_methods(["GET", "DELTE", "PUT"])
# def api_show_shoe(request, id):
#     if request.method == "GET":
#         try:
#             shoe = Shoe.objects.get(id=id)
#             return JsonResponse(
#                 shoe,
#                 encoder=ShoeDetailEncoder,
#                 safe=False,
#             )
#         except Shoe.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
#     elif request.method == "DELETE":
#         try:
#             shoe = Shoe.objects.get(id=id)
#             shoe.delete()
#             return JsonResponse(
#                 shoe,
#                 encoder=ShoeDetailEncoder,
#                 safe=False,
#             )
#         except Shoe.DoesNotExist:
#             return JsonResponse({"message": "Does not exist"})
#     else: #PUT
#         try:
#             content = json.loads(request.body)
#             shoe = Shoe.objects.get(id=id)

#             props = ["manufacturer", "model_name", "color", "picture"]
#             for prop in props:
#                 if prop in content:
#                     setattr(shoe, prop, content[prop])
#             shoe.save()
#             return JsonResponse(
#                 shoe,
#                 encoder=ShoeListEncoder,
#                 safe=False,
#             )
#         except Shoe.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response




# @require_http_methods(["GET", "POST"])
# def api_list_shoes(request, bin_vo_id=None):

#     if request.method == "GET":
#         if bin_vo_id is not None:
#             shoes = Shoe.objects.filter(bin=bin_vo_id)
#         else:
#             shoes = Shoe.objects.all()

#         return JsonResponse(
#             {"attendees": shoes},
#             encoder=ShoeListEncoder,
#         )
#     else:
#         content = json.loads(request.body)

#         # Get the Conference object and put it in the content dict
#         try:
#             bin_number = content["bin"]
#             shoe = BinVO.objects.get(closet_name=bin_number)
#             content["shoe"] = shoe
#         except BinVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid shoe id"},
#                 status=400,
#             )

#         shoes = Shoe.objects.create(**content)
#         return JsonResponse(
#             shoes,
#             encoder=ShoeDetailEncoder,
#             safe=False,
#         )
