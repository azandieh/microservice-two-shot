from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number"
    ]

class HatListModelEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "style_name",
        "color",
        "picture",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailModelEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "style_name",
        "color",
        "picture",
        "location",
        "id"
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


# @require_http_methods(["GET", "POST"])
# def hat_list(request):
#     if request.method == "GET":
#         hats = Hat.objects.all()
#         return JsonResponse(
#             {"hats": hats},
#             encoder=HatListModelEncoder,
#         )
#     else:
#         content = json.loads(request.body)
#         hat = Hat.objects.create(**content)
#         return JsonResponse(
#             hat,
#             encoder=HatListModelEncoder,
#             safe=False,
#         )


@require_http_methods(["GET", "POST"])
def hat_list(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListModelEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListModelEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def hat_detail(request, id):
    if request.method == "GET":
        try:
            hats = Hat.objects.get(id=id)
            return JsonResponse(
                hats,
                encoder=HatDetailModelEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hats = Hat.objects.get(id=id)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatDetailModelEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
