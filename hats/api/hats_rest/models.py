from django.db import models

# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=250, unique=True, null=True)
    closet_name = models.CharField(max_length=150)
    section_number = models.PositiveSmallIntegerField(blank=True, null=True)
    shelf_number = models.PositiveSmallIntegerField(blank=True, null=True)

    def __str__(self):
        return self.closet_name


class Hat(models.Model):
    name = models.CharField(max_length=150, null=True)
    fabric = models.CharField(max_length=150)
    style_name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.fabric
