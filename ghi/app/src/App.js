import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';
import HatForm from './HatForm';
import HatList from './HatList';


function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={< HatList hats={props.hats} /> } />
            <Route path="new" element={<HatForm/>} />
          </Route >
          <Route path="Shoes" >
            <Route index element={< ShoeList shoes={props.shoes}/>} />
            <Route path="new" element={< ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
