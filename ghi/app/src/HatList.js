import { Link } from 'react-router-dom';
import { React, useState, useEffect } from 'react';

async function HatDelete(hat) {
    const HatDeleteURL = `http://localhost:8090/api/hats/${hat.id}`
    const fetchConfig = {
        method: "delete"
    }
    const response = await fetch(HatDeleteURL, fetchConfig);
    if (response.ok) {
        const data = await response.json()
        window.location.reload();
    }
}


function HatList() {
    const [hats, setHats] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);


    return (
        <div>
            <br />
            <div className='d-grid gap-5 d-sm-flex justify-content-sm-center'>
                <Link to='/hats/new' className='btn btn-primary btn-lg px-4 gap-3'>Click here to create a Hat</Link>
            </div>
            <br />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Style Name</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr key={hat.id}>
                                <td>{ hat.name }</td>
                                <td>{ hat.style_name }</td>
                                <td>{ hat.fabric }</td>
                                <td>{ hat.color }</td>
                                <td><img src={ hat.picture } width="50" height="50" /></td>
                                <td>{ hat.location.closet_name }</td>
                                <td><button onClick={() => HatDelete(hat)}>Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default HatList;
