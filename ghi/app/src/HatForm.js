import { React, useEffect, useState } from 'react';

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [style_name, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.fabric = fabric;
        data.style_name = style_name;
        data.color = color;
        data.picture = picture;
        data.location = location;
        console.log(data);

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            setName('');
            setFabric('');
            setStyleName('');
            setColor('');
            setPicture('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className='form-floating mb-3'>
                            <input onChange={handleNameChange} value={name} placeholder='name' required type="text" name="name" id="name" className='form-control'/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFabricChange} value={fabric} placeholder='fabric' required type="text" name="fabric" id="fabric" className='form-control'/>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleStyleNameChange} value={style_name} placeholder='style_name' required type="text" name="style_name" id="style_name" className='form-control'/>
                            <label htmlFor='style_name'>Style name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleColorChange} value={color} placeholder='color' required type="text" name="color" id="color" className='form-control'/>
                            <label htmlFor='color'>Color</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handlePictureChange} value={picture} placeholder='picture' required type="link" name="picture" id="picture" className='form-control'/>
                            <label htmlFor='picture'>Picture url</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <select onChange={handleLocationChange} value={location} name="location" id="location" className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.href} value={location.href}>
                                        {location.closet_name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>

    );
}
export default HatForm;
