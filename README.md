# Wardrobify

Team:

* Person 1 - Aurash Z
* Person 2 - Robin Kim


## Design

## Shoes microservice

Will look at the code and put in more code as needed.

## Hats microservice

Create models according to the instructions and create views that grabs use the models and create functions that can get, post, and delete.
Then after, we create the form js and create a list js to display each shoe as well as integrate the form inside the list js.


Git Process
Person 1 who adds code:
1. Add code - add, commit, push origin branch-name
2. git checkout main
3. git merge branch
4. git push

Person 2 who retrieves code:
1. git checkout main
2. git pull
3. git checkout (aurash-branch or robin-branch)
4. git merge main
